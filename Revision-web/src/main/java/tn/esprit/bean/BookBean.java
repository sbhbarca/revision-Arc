package tn.esprit.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.entities.Book;
import tn.esprit.services.LocalBook;
import tn.esprit.services.RemoteBook;

@ManagedBean
@SessionScoped
public class BookBean {

	@EJB
	LocalBook localBook;
	
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	private List<Book> books;
	
	private Book book;
	
	@PostConstruct
	public void init(){
		book = new Book();
		books = localBook.findAllBooks();
	}
	
	public String doAdd(){
		String navigateTo = "/admin/addLivre2?faces-redirect=true";
		
		
		return navigateTo;
	}
	public String doAdd2(){
		String navigateTo = "/admin/addLivre3?faces-redirect=true";
		
		
		return navigateTo;
	}
	
	public String back(){
		String navigateTo = "/admin/addLivre1?faces-redirect=true";
		
		
		return navigateTo;
	}
	
	public String addBook(){
		
		localBook.addBook(book);
		books.add(book);
		book = new Book();
		
		String navigateTo = "/admin/welcome?faces-redirect=true";
		
		
		return navigateTo;
	}
	
	public String cancelBook(){

		book = new Book();
		
		String navigateTo = "/admin/welcome?faces-redirect=true";
		
		
		return navigateTo;
	}
	
}
