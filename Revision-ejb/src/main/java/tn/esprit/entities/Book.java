package tn.esprit.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Book implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idBook;
	private String title;
	private String description;
	private Float price;
	private Integer nbOfpage;
	private boolean illustrations;
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Integer getNbOfpage() {
		return nbOfpage;
	}
	public void setNbOfpage(Integer nbOfpage) {
		this.nbOfpage = nbOfpage;
	}
	public boolean isIllustrations() {
		return illustrations;
	}
	public void setIllustrations(boolean illustrations) {
		this.illustrations = illustrations;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne
	private User user;
}
