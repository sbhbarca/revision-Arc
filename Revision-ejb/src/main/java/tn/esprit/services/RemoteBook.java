package tn.esprit.services;

import javax.ejb.Remote;

import tn.esprit.entities.Book;

@Remote
public interface RemoteBook {
	
	public void addBook(Book book);

}
