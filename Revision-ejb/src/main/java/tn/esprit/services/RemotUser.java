package tn.esprit.services;

import javax.ejb.Remote;

import tn.esprit.entities.User;

@Remote
public interface RemotUser {

	public void addUser(User u);
}
