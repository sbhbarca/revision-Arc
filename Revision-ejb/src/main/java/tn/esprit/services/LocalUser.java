package tn.esprit.services;

import javax.ejb.Local;

import tn.esprit.entities.User;

@Local
public interface LocalUser {

	public User authentification(String login, String password);
}
