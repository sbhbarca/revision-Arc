package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Book;

@Local
public interface LocalBook {
	
	public List<Book> findAllBooks();
	public void addBook(Book book);

}
