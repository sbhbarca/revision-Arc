package tn.esprit.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.entities.User;

@Stateless
public class UserService implements LocalUser, RemotUser {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addUser(User u) {
		// TODO Auto-generated method stub
		em.persist(u);
	}

	@Override
	public User authentification(String login, String password) {
		// TODO Auto-generated method stub
		User u = null;
		Query req= em.createQuery("select u from User u where u.login like :log and u.password like :pwd");
		req.setParameter("log", login).setParameter("pwd", password);
		try {
			u = (User) req.getSingleResult();
		} catch (NoResultException e) {
			// TODO: handle exception
			System.out.println("No data Found");
		}
		return u;
	}

}
