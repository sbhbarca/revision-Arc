package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Book;

@Stateless
public class BookService implements LocalBook, RemoteBook{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		em.persist(book);
	}

	@Override
	public List<Book> findAllBooks() {
		// TODO Auto-generated method stub
		TypedQuery<Book> req = em.createQuery("select b from Book b",Book.class);
		return req.getResultList();
	}

}
